name := "MyTest"

version := "0.1"

scalaVersion := "2.11.8"

val scalatest = "3.0.4"


libraryDependencies ++= Seq(
  "org.scalatest"                      %% "scalatest"                  % scalatest % "test"
)