# MyEvaluationRepo

## Key Notes
 - `MyDriver` is the driver file. You can simply run that driver class to run against a randomly generated sequence of tuples.
 - `MyTupleCompressorTest` covers tests for several cases
 - If you turn debug on you can see the input, grouped input and no of iterations happening. 
 - The solution given worst case `O(n^2)` when all tuples are non overlapping and `O(n)` if the first tuple turns out to be of the largest range (which means every time it loops the linkedlist size reduces becasue the nodes are merged) in time.
 - For huge input if you check `MyTupleCompressor` class, the compressor does a batch processing so it should scale pretty well.
 - It can merge tuples input in any order eg (1,4),(3,5),(20,1) will give (1,5),(20,1) and (3,4),(3,5),(1,3) will give (1, 5)
 
## How to run?
 - Download the jar `MyTest-assembly-0.1.jar`
 - Use command `java -jar MyTest-assembly-0.1.jar`
 - You will be prompted to enter the size of the input and enter `true` or `false` for debug on or off in the following prompt
 


