

object MyDriver {
  def main(args: Array[String]): Unit = {
    println("Enter the size of tuple seq to run on: ")
    val a= scala.io.StdIn.readInt()
    println("Do you want debug on?")
    val b =scala.io.StdIn.readBoolean()
    val myInputGenerator = new MyInputGenerator(a)
    val myTupleCompressor = new MyTupleCompressor
    driver(myInputGenerator, myTupleCompressor, b)
  }

  def driver(inputGenerator: MyInputGenerator, myTupleCompressor: MyTupleCompressor, debug: Boolean) =  {
    val input = inputGenerator.generateInput()
    println(s"Input is ${input.mkString(",")}")

    if(debug) {
      println(input.groupBy(_._1).toSeq.sortBy(_._1).mkString("\n"))
    }

    val res = myTupleCompressor.compressTuples(input, debug)
    println("Compressed output is " + res.mkString(","))
  }
}
