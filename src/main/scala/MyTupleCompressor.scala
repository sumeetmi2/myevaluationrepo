import java.util

import scala.collection.JavaConverters._

class MyTupleCompressor {
  def compressTuples(tups: Seq[(Int, Int)], debug: Boolean = false): Seq[(Int, Int)] = {
    val tmpResults =
    tups.grouped(100).toList.par.flatMap {
      xx => internalCompressTuples(xx, debug)
    }.toList
    internalCompressTuples(tmpResults, debug)
  }

  private def internalCompressTuples(tups: Seq[(Int, Int)], debug: Boolean = false) = {
    var linkedList = new java.util.LinkedList[(Int, Int)]()
    var cnt = 0
    for(t <- tups) {
      cnt = cnt + 1
      if(linkedList.isEmpty) {
        // if linkedlist is empty add the first tuple
        linkedList.add(t)
      }  else {
        val itr = linkedList.listIterator()
        var tt = itr.next()

        while (!isInBetween(t, tt) && itr.hasNext) {
          // iterate till we find incoming tuple is inbetween the range of the tuple in the visiting node
          tt = itr.next()
          cnt = cnt + 1
        }

        var xx = t
        if(isInBetween(t, tt)) {
          // merge the current node's tuple with the incoming tuple
          xx = mergeTuple(tt, t)
          // remove current tuple
          itr.remove()
        }
        // add the updated tuple
        itr.add(xx)
        var itr2 = linkedList.listIterator()
        val prev = itr2.next()
        while (itr2.hasNext) {
          cnt = cnt + 1
          val curr = itr2.next()
          if(!compareCurrentAndNextTuple(prev, curr)) {
            // merge prev and next node if they are like (1,5)->(4,3)->(11,12) to one node (1,5)->(11,12)
            val pp = mergeTuple(prev, curr)
            itr2.remove()
            if(itr2.hasNext) {
              itr2.next()
              itr2.remove()
              itr2.add(pp)
            } else {
              // if we reach head of the linked list update the head
              linkedList = new util.LinkedList[(Int, Int)]()
              linkedList.add(pp)
            }
          }
        }
      }
    }
    if(debug) {
      println(s"for input of size ${tups.size} no of loops: $cnt")
    }
    linkedList.asScala
  }

  private def compareCurrentAndNextTuple(t1: (Int, Int), t2: (Int, Int)): Boolean = {
    t1._2 < t2._1
  }

  private def mergeTuple(t1: (Int, Int), t2: (Int, Int)): (Int, Int) = {
    (Math.min(t1._1, t2._1), Math.max(t1._2, t2._2))
  }

  private def isInBetween(t1: (Int, Int), t2: (Int, Int)): Boolean = {
    (t2._1 <= t1._1 &&  t1._1 <= t2._2) || (t1._1 <= t2._1 && t2._1 <= t1._2)
  }

}
