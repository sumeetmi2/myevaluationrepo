import scala.util.Random

class MyInputGenerator(sizeOfInput: Int) {

  def generateInput(): Seq[(Int, Int)] = {
    (1 to sizeOfInput).map {
      i =>
        val rnd1 = new Random()
        rnd1.nextInt(10)
        val i1 = rnd1.nextInt(10) + 1
        val rnd2 = new Random()
        val i2 = rnd2.nextInt(10) + i1
        (i1, i2)
    }
  }

}
