import org.scalatest.FunSuite

class MyInputGeneratorTest extends FunSuite{
  val testSize = 19
  val inputGenerator = new MyInputGenerator(testSize)
  test("should generate random tuple sequence of given size") {
    assert(inputGenerator.generateInput().size == testSize)
  }
}
