import org.scalatest.{FunSuite, Matchers}

class MyTupleCompressorTest extends FunSuite with Matchers{
  val myTupleCompressor = new MyTupleCompressor

  test("test1 - should compress the input sequence of tuples to correct values") {
    val inputSeq = Seq((1,4) , (5,10) , (5,12) , (3, 6), (13, 15))
    val outputSeq = Seq((1,12), (13, 15))
    myTupleCompressor.compressTuples(inputSeq, true) should contain theSameElementsAs outputSeq
  }

  test("test2 - should compress the input sequence of tuples to correct values") {
    val inputSeq = Seq((5,10) , (5,12) , (3, 6), (1,4))
    val outputSeq = Seq((1,12))
    myTupleCompressor.compressTuples(inputSeq, true) should contain theSameElementsAs outputSeq
  }

  test("test3 - should compress the input sequence of tuples to correct values") {
    val inputSeq = Seq((1,4), (5,10) , (5,12))
    val outputSeq = Seq((1,4), (5,12))
    myTupleCompressor.compressTuples(inputSeq, true) should contain theSameElementsAs outputSeq
  }

  test("test4 - should compress the input sequence of tuples to correct values") {
    val inputSeq = Seq((1,4), (4,4))
    val outputSeq = Seq((1,4))
    myTupleCompressor.compressTuples(inputSeq, true) should contain theSameElementsAs outputSeq
  }

  test("test5 - should compress the input sequence of tuples to correct values") {
    val inputSeq = Seq((1,4), (1,4))
    val outputSeq = Seq((1,4))
    myTupleCompressor.compressTuples(inputSeq, true) should contain theSameElementsAs outputSeq
  }

  test("test6 - should compress the input sequence of tuples to correct values") {
    val inputSeq = Seq.empty[(Int, Int)]
    val outputSeq = Seq.empty[(Int, Int)]
    myTupleCompressor.compressTuples(inputSeq, true) should contain theSameElementsAs outputSeq
  }

}
